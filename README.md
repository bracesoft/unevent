UNevent
Copyright 2019 BraceSoft

UNevent is a free and open source software to get, manage, store and analyse
registration data, that occures in small to big events. With forms, templates
and an optimized layout, UNevent is the perfect solution for businesses.