import os
import sys

#import about
#import update
#import event
#import eventsetup

from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QAction
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot

class launcherWindow (QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = "UNevent - Launcher"
        self.left = 500
        self.top = 500
        self.width = 1000
        self.height = 1000
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)

        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu("   File   ")
        eventMenu = mainMenu.addMenu("   Event   ")
        templateMenu = mainMenu.addMenu("   Template   ")
        helpMenu = mainMenu.addMenu("   Help   ")

        file_settings_Button = QAction(QIcon("exit24.png"), "Settings",self)
        file_settings_Button.triggered.connect(self.file_settings)
        fileMenu.addAction(file_settings_Button)

        file_update_Button = QAction(QIcon("exit24.png"), "Update",self)
        file_update_Button.triggered.connect(self.file_update)
        fileMenu.addAction(file_update_Button)

        file_about_Button = QAction(QIcon("exit24.png"), "About",self)
        file_about_Button.triggered.connect(self.file_about)
        fileMenu.addAction(file_about_Button)

        file_end_Button = QAction(QIcon("exit24.png"), "End",self)
        file_end_Button.triggered.connect(self.file_end)
        fileMenu.addAction(file_end_Button)

        event_new_Button = QAction(QIcon("exit24.png"), "Start new",self)
        event_new_Button.triggered.connect(self.event_new)
        eventMenu.addAction(event_new_Button)

        event_continue_Button = QAction(QIcon("exit24.png"), "Continue",self)
        event_continue_Button.triggered.connect(self.event_continue)
        eventMenu.addAction(event_continue_Button)

        event_unite_Button = QAction(QIcon("exit24.png"), "Unite",self)
        event_unite_Button.triggered.connect(self.event_unite)
        eventMenu.addAction(event_unite_Button)

        event_convert_Button = QAction(QIcon("exit24.png"), "Convert",self)
        event_convert_Button.triggered.connect(self.event_convert)
        eventMenu.addAction(event_convert_Button)

        event_get_bkp_Button = QAction(QIcon("exit24.png"), "Get Backup Data",self)
        event_get_bkp_Button.triggered.connect(self.event_get_bkp)
        eventMenu.addAction(event_get_bkp_Button)

        template_new_Button = QAction(QIcon("exit24.png"), "New",self)
        template_new_Button.triggered.connect(self.template_new)
        templateMenu.addAction(template_new_Button)

        template_edit_Button = QAction(QIcon("exit24.png"), "Edit",self)
        template_edit_Button.triggered.connect(self.template_edit)
        templateMenu.addAction(template_edit_Button)

        template_import_Button = QAction(QIcon("exit24.png"), "Import",self)
        template_import_Button.triggered.connect(self.template_import)
        templateMenu.addAction(template_import_Button)

        help_online_Button = QAction(QIcon("exit24.png"), "Online-Help",self)
        help_online_Button.triggered.connect(self.help_online)
        helpMenu.addAction(help_online_Button)

        help_manual_Button = QAction(QIcon("exit24.png"), "Manual",self)
        help_manual_Button.triggered.connect(self.help_manual)
        helpMenu.addAction(help_manual_Button)

        help_web_Button = QAction(QIcon("exit24.png"), "Web",self)
        help_web_Button.triggered.connect(self.help_web)
        helpMenu.addAction(help_web_Button)

        help_readme_Button = QAction(QIcon("exit24.png"), "Readme",self)
        help_readme_Button.triggered.connect(self.help_readme)
        helpMenu.addAction(help_readme_Button)

        help_credits_Button = QAction(QIcon("exit24.png"), "Credits",self)
        help_credits_Button.triggered.connect(self.help_credits)
        helpMenu.addAction(help_credits_Button)

        self.show()

    def file_settings (self):
        import webbrowser
        #webbrowser.open("config.txt")
        launcherWindow.messagebox("Please restart UNevent to apply all changes!")

    def file_update (self):
        input()

    def file_about (self):
        about.start()

    def file_end (self):
        sys.exit()

    def event_new (self):
        input()

    def event_continue (self):
        input()

    def event_unite (self):
        input()

    def event_convert (self):
        input()

    def event_get_bkp (self):
        import webbrowser
        webbrowser.open(os.path.dirname(__file__) + "/backup/")

    def template_new (self):
        input()

    def template_edit (self):
        input()

    def template_import (self):
        input()

    def help_online (self):
        import webbrowser
        webbrowser.open(domain + "help")

    def help_manual ():
        import webbrowser
        webbrowser.open(domain + "manual.pdf")

    def help_web (self):
        import webbrowser
        webbrowser.open(domain)

    def help_readme (self):
        import webbrowser
        webbrowser.open("README.md")

    def help_credits (self):
        import webbrowser
        webbrowser.open("CONTRIBUTING.md")


def init ():

    global application
    global version
    global edition

    application = "UNevent"
    version = 100
    edition = "free"


if __name__ == "__main__":
    init()
    app = QApplication(sys.argv)
    ex = launcherWindow()
    sys.exit(app.exec_())
